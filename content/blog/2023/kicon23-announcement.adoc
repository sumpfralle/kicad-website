+++
title = "KiCad Conference 2023 Announcement"
date = "2023-03-28"
draft = false
aliases = [
    "/post/kicon23-announcement/"
]
"blog/categories" = [
    "News"
]
+++

The KiCad project is excited to announce the second annual KiCad Conference
https://kicon.kicad.org/[(KiCon)] in A Coruña, Spain from Sept 9th through the
10th, 2023.  The conference will be held at the https://www.palexco.com/[Palexco Conference Center].

This conference is geared towards KiCad users.  There will be
talks from users of all skill levels as well as members of the
KiCad Lead Development Team.
The conference keynote talk will be given by Wayne Stambaugh, the KiCad project leader.

Come join enthusiastic KiCad users and developers from all over Europe and the world
for a fun and informative two full days of all things KiCad.  On top of
interesting and insightful talks, there will be plenty of time to share
ideas and get to know other users and members of the development team.
Attendees can expect to receive an excellent badge and some nifty swag.

If you are interested in sponsoring the event, please go to the
https://kicon.kicad.org/sponsors/sponsors-prospectus/[sponsors page] to donate.  If you are
interested in volunteering please go to the
https://forms.gle/JcwJqPZhNTzmKUuv8[volunteers page]
to sign up.  Any help is greatly appreciated.  Don't forget to thank all
our sponsors and volunteers when you attend KiCon.  It's thanks to their
efforts that an event like KiCon can even happen.

Tickets went on sale on March 28th.  Get your tickets today and come
join us for two days of fun and learning with KiCad.  All are welcome to
the what we hope to be a yearly conference.

We are looking forward to seeing you at KiCon!

The KiCad Team
