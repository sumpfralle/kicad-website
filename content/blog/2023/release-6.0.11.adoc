+++
title = "KiCad 6.0.11 Release"
date = "2023-01-26"
draft = false
"blog/categories" = [
    "Release Notes"
]
+++

The KiCad project is proud to announce the latest and most likely last
series 6 stable release.  The 6.0.11 stable version contains critical bug
fixes and other minor improvements since the previous release.

<!--more-->

A list of all of the fixed issues since the 6.0.10 release can be found
on the https://gitlab.com/groups/kicad/-/milestones/22[KiCad 6.0.11
milestone] page. This release contains several critical bug fixes so please
consider upgrading as soon as possible.

Version 6.0.11 is made from the
https://gitlab.com/kicad/code/kicad/-/commits/6.0/[6.0] branch with
some cherry picked changes from the development branch.

Packages for Windows, macOS, and Linux are available or will be
in the very near future.  See the
link:/download[KiCad download page] for guidance.

Thank you to all developers, packagers, librarians, document writers,
translators, and everyone else who helped make this release possible.

== Changelog

=== General
- Fix PDF plot text dimensions. https://gitlab.com/kicad/code/kicad/-/issues/12159[#12159]
- https://gitlab.com/kicad/code/kicad/-/commit/cc0b51c81a5fa0691c8e0ba3702b4614e6d66467[Ensure 3D model viewer is correctly initialized.]

=== Schematic Editor
- Fix ERC crash. https://gitlab.com/kicad/code/kicad/-/issues/13256[#13256]
- Fix crash when importing Eagle schematic. https://gitlab.com/kicad/code/kicad/-/issues/13215[#13215]
- Fix crash when changing language and the simulator window is open. https://gitlab.com/kicad/code/kicad/-/issues/13349[#13349]
- Fix broken graphic import file extension filters for some locales. https://gitlab.com/kicad/code/kicad/-/issues/13570[#13570]
- Fix project rescue when symbol name cases do not match. https://gitlab.com/kicad/code/kicad/-/issues/13602[#13602]
- https://gitlab.com/kicad/code/kicad/-/commit/4e34bdb621732b68a139a8d33d20f6a5ea7c1044[Enable real time connectivity by default.]

=== Symbol Editor
- Prevent forward slash character in symbol name from corrupting symbol library. https://gitlab.com/kicad/code/kicad/-/issues/13504[#13504]

=== Footprint Editor
- Do not prune layers when copying items to the clipboard. https://gitlab.com/kicad/code/kicad/-/issues/13334[#13334]

=== Windows
- https://gitlab.com/kicad/code/kicad/-/commit/6c0a30e2f3ac5064e6550b011149fc90476db904[Use older version of curl that works correctly].
