+++
title = "KiCad 7.0.8 Release"
date = "2023-09-30"
draft = false
"blog/categories" = [
    "Release Notes"
]
+++

The KiCad project is proud to announce the version 7.0.8 bug fix release.
The 7.0.8 stable version contains critical bug fixes and other minor
improvements since the previous release.

<!--more-->

A list of all of the fixed issues since the 7.0.7 release can be found
on the https://gitlab.com/groups/kicad/-/milestones/30[KiCad 7.0.8
milestone] page. This release contains several critical bug fixes so please
consider upgrading as soon as possible.

Version 7.0.8 is made from the
https://gitlab.com/kicad/code/kicad/-/commits/7.0/[7.0] branch with
some cherry picked changes from the development branch.

Packages for Windows, macOS, and Linux are available or will be
in the very near future.  See the
link:/download[KiCad download page] for guidance.

Thank you to all developers, packagers, librarians, document writers,
translators, and everyone else who helped make this release possible.

== Changelog

=== General
- Disable "Override individual item colors" on read-only themes. https://gitlab.com/kicad/code/kicad/-/issues/15426[#15426]
- Fix text control width when displaying number with many digits. https://gitlab.com/kicad/code/kicad/-/issues/15539[#15539]
- Fix crash when using overbar with custom font. https://gitlab.com/kicad/code/kicad/-/issues/15508[#15508]
- https://gitlab.com/kicad/code/kicad/-/commit/702e08b083b952dee409b089eb02286727bfe6b6[Fix constant data collection prompts because user settings were getting erased constantly]
- https://gitlab.com/kicad/code/kicad/-/commit/955313e97d9c0fa9174bae376c1d545b8a2adac5[Fix locale issues in graphics abstraction layer settings panel].
- Include underscore characters in word search. https://gitlab.com/kicad/code/kicad/-/issues/14779[#14779]
- Improve error reporting when running Python action plugins. https://gitlab.com/kicad/code/kicad/-/issues/15520[#15520]

=== Schematic Editor
- Fix crash when fetching symbols from database libraries. https://gitlab.com/kicad/code/kicad/-/issues/15315[#15315]
- Fix broken bus alias definition entry behavior. https://gitlab.com/kicad/code/kicad/-/issues/15320[#15320]
- Don't stop drawing wire on unfold from bus operation. https://gitlab.com/kicad/code/kicad/-/issues/15313[#15313]
- https://gitlab.com/kicad/code/kicad/-/commit/c468b68c5ad0ca0e9151d9a464d76616148539d8[Improve handling of dangling markers during bus unfold].
- Fix incorrect thermal relief spoke count. https://gitlab.com/kicad/code/kicad/-/issues/15280[#15280]
- https://gitlab.com/kicad/code/kicad/-/commit/8f57fd82a036ad699167c91514cc7aa8c879adfb[Provide more useful feedback when a database table is misconfigured].
- Fix hang when opened if last shown on a different monitor. https://gitlab.com/kicad/code/kicad/-/issues/11850[#11850]
- Make label user field selectable when using a custom font. https://gitlab.com/kicad/code/kicad/-/issues/15165[#15165]
- https://gitlab.com/kicad/code/kicad/-/commit/fd049ef510aa6af70621fa6568d514338f360061[Fix selection shadow drawing for global labels].
- Fix graphical glitch after undo of sheet pin move. https://gitlab.com/kicad/code/kicad/-/issues/15129[#15129]
- Properly highlight global and hierarchical label selections. https://gitlab.com/kicad/code/kicad/-/issues/15461[#15461]
- Fix crash when changing symbol simulation field name visibility. https://gitlab.com/kicad/code/kicad/-/issues/15470[#15470]
- Remove duplicate references from change symbol dialog. https://gitlab.com/kicad/code/kicad/-/issues/15480[#15480]
- Prevent infinite loop hang when removing sheet instance properties. https://gitlab.com/kicad/code/kicad/-/issues/15498[#15498]
- https://gitlab.com/kicad/code/kicad/-/commit/76a3b3db971136977519cf86df9d34b48db80c5e[Improve handling of large number of duplicate references].
- https://gitlab.com/kicad/code/kicad/-/commit/f9b59b4fdf2ce353f2d368eabf34f1924dd786e4[Prevent setup dialog crash due to missing severities panel initialization].
- Don't count pins of both body style when matching to footprint pads. https://gitlab.com/kicad/code/kicad/-/issues/15550[#15550]
- Re-run automatic field placement after symbol update when appropriate. https://gitlab.com/kicad/code/kicad/-/issues/15541[#15541]
- Save ERC settings and exclusions when saving schematic. https://gitlab.com/kicad/code/kicad/-/issues/15274[#15274]
- Fix symbol name comparison when name contains forward slash characters. https://gitlab.com/kicad/code/kicad/-/issues/15540[#15540]
- Support SHORT_NET_NAME, NET_NAME, NET_CLASS, and PIN_NAME variable expansion. https://gitlab.com/kicad/code/kicad/-/issues/15544[#15544]
- Fix repeat item bug when moving, duplicating or copy-pasting an item. https://gitlab.com/kicad/code/kicad/-/issues/15556[#15556]
- Honor override individual item colors setting for filled shapes. https://gitlab.com/kicad/code/kicad/-/issues/15572[#15572]
- https://gitlab.com/kicad/code/kicad/-/commit/6ee909b95809527f8a6bed0178142ee162e2e868[Support custom sheet sizes when importing Altium schematic].
- Work around wxGTK printing offset. https://gitlab.com/kicad/code/kicad/-/issues/1907[#1907]
- Fix OpenGL bug when importing non-KiCad schematic. https://gitlab.com/kicad/code/kicad/-/issues/15159[#15159]
- https://gitlab.com/kicad/code/kicad/-/commit/951fd2d6935bbc95055e07f92ff7f86abe716b09[Support line dash styles and colors when importing Altium schematic].
- Fix connectivity regression. https://gitlab.com/kicad/code/kicad/-/issues/14818[#14818]
- Fix crash when using ${FOOTPRINT_NAME} or ${FOOTPRINT_LIBRARY} variables. https://gitlab.com/kicad/code/kicad/-/issues/15676[#15676]
- https://gitlab.com/kicad/code/kicad/-/commit/23fab89131187bcb234b2a26e3b9573fa19490cc[Properly handle incremental bus connections].

=== Simulator
- https://gitlab.com/kicad/code/kicad/-/commit/95e6be0e8138841b6ac66fc715b6eee5ec2645ac[Prevent crash trying to load missing spice library].

=== Board Editor
- https://gitlab.com/kicad/code/kicad/-/commit/8451e6dcf1ac7bab3319be4c5bd808883152804e[Fix failure on multiple DRC runs via scripting].
- https://gitlab.com/kicad/code/kicad/-/commit/aa1910a8342cfc046a28e0cad6836e3d284f1775[Implement hole clearance checking in router].
- Fix crash when tuning the length of a differential pair. https://gitlab.com/kicad/code/kicad/-/issues/15268[#15268]
- Fix bug when resolving clearance between pad and zone using custom rules. https://gitlab.com/kicad/code/kicad/-/issues/15233[#15233]
- Fix crash when attempting to delete dimensions in a footprint. https://gitlab.com/kicad/code/kicad/-/issues/15418[#15418]
- Fix STEP export for missing curve geometry. https://gitlab.com/kicad/code/kicad/-/issues/14910[#14910]
- https://gitlab.com/kicad/code/kicad/-/commit/246be732a19de06d2e23f9760ed769dffba94ace[Ensure the correct spoke angle is displayed in the pad properties dialog].
- Improve fidelity for thermal spokes when importing Eagle boards. https://gitlab.com/kicad/code/kicad/-/issues/15428[#15428]
- Save invalid outline DRC error exclusion setting. https://gitlab.com/kicad/code/kicad/-/issues/15415[#15415]
- Fix missing top side holes when exporting to STEP. https://gitlab.com/kicad/code/kicad/-/issues/15467[#15467]
- Prevent crash when a plugin Python script tries to access the board editor frame too early. https://gitlab.com/kicad/code/kicad/-/issues/15414[#15414]
- Compute VRML export bounding box with "origin at pcb center" using only the edge layer. https://gitlab.com/kicad/code/kicad/-/issues/15472[#15472]
- Fix accidental plotting of disabled textbox borders. https://gitlab.com/kicad/code/kicad/-/issues/15252[#15252]
- https://gitlab.com/kicad/code/kicad/-/commit/3e1309110e8caa5a61b0db0f7e86a5fea1f09aaa[Fix crash loading a polygon when importing EAGLE board].
- https://gitlab.com/kicad/code/kicad/-/commit/33c2005c18e195358fdf604b4dfa1974d3d2506f[Fix crash when loading unknown signal class name when importing EAGLE board].
- https://gitlab.com/kicad/code/kicad/-/commit/96723901049b08bc07ea57ff6e644495d520a140[Fix a search pane crash when a row isn't in the hit list].
- Handle dimensions and textboxes when plotting contours to DXF. https://gitlab.com/kicad/code/kicad/-/issues/11901[#11901]
- Correctly handle DXF arcs import with inverted coordinate system. https://gitlab.com/kicad/code/kicad/-/issues/14905[#14905]
- Don't snap a footprint or group to its children. https://gitlab.com/kicad/code/kicad/-/issues/15535[#15535]
- Use 90 degree spokes when loading pre-7.0 files with custom pads with round anchors. https://gitlab.com/kicad/code/kicad/-/issues/15518[#15518]
- Fix interactive router clearance violations around custom-shaped pads. https://gitlab.com/kicad/code/kicad/-/issues/15553[#15553]
- Fix crash when activating "Pack & Move" during active move operation. https://gitlab.com/kicad/code/kicad/-/issues/15545[#15545]
- Fix unexpected behavior when pressing G or D ("drag" hotkeys) while mouse-dragging. https://gitlab.com/kicad/code/kicad/-/issues/15312[#15312]
- Remove up/down buttons from plot dialog plot on all layers selection until we implement layer ordering. https://gitlab.com/kicad/code/kicad/-/issues/14574[#14574]
- Fix crash when duplicating then rotating a footprint. https://gitlab.com/kicad/code/kicad/-/issues/15487[#15487]
- Correctly import polygon cutout in Altium importer. https://gitlab.com/kicad/code/kicad/-/issues/15587[#15587]
- Don't include plating thickness when drawing hole clearance lines. https://gitlab.com/kicad/code/kicad/-/issues/15575[#15575]
- https://gitlab.com/kicad/code/kicad/-/commit/0d9d8c28f1a50ac58a18c319a608d1d47aa2aba3[Flip view port when flip board is enabled].
- Fix DRC crash. https://gitlab.com/kicad/code/kicad/-/issues/15604[#15604]
- Fix reading/writing thermal spoke angles for custom pads. https://gitlab.com/kicad/code/kicad/-/issues/15518[#15518]
- Fix oval pad snapping. https://gitlab.com/kicad/code/kicad/-/issues/15594[#15594]
- Fix arc coordinate changes when saving unmodified board. https://gitlab.com/kicad/code/kicad/-/issues/15073[#15073]
- https://gitlab.com/kicad/code/kicad/-/commit/72737b20cd83e8c788c1e209aa88d5a20230db79[Fix reading legacy zone fills which were based on stroke filled polygons].
- Fix issues with very small arcs. https://gitlab.com/kicad/code/kicad/-/issues/15639[#15639]
- Properly handle impedance control in board setup dialog. https://gitlab.com/kicad/code/kicad/-/issues/15690[#15690]

=== Footprint Editor
- Honor pad offset when creating custom pad anchors and polygons. https://gitlab.com/kicad/code/kicad/-/issues/15494[#15494]
- https://gitlab.com/kicad/code/kicad/-/commit/480a9ac77dd9f17e9b083ea05c846f7f4d453094[Implement undo/redo when using pad edit mode changes].
- Bug fixes for paste margins on custom-shaped pads. https://gitlab.com/kicad/code/kicad/-/issues/15125[#15125]
- https://gitlab.com/kicad/code/kicad/-/commit/8f58e1a9e00ae06e15e84a230bb6fc323586740b[Print the command line sent to plot STEP file in dialog report window].

=== Footprint Library Viewer
- Remember library list pane width. https://gitlab.com/kicad/code/kicad/-/issues/15500[#15500]
- Correctly retain pad anchor when converting custom pad shape to polygon. https://gitlab.com/kicad/code/kicad/-/issues/15555[#15555]

=== Gerber Viewer
- Fix incorrect export of flashed shapes on non copper layers when exporting to PCB. https://gitlab.com/kicad/code/kicad/-/issues/15613[#15613]

=== 3D Viewer
- https://gitlab.com/kicad/code/kicad/-/commit/26b8812f15798f47c5f32725a0f9a20bea2c2596[Fix rendering disabled text box borders].
- https://gitlab.com/kicad/code/kicad/-/commit/058a0bbf93526f3e7beba03da86619aca7c255ac[Fix crash attempting to render degenerate 3D shapes].
- Fix mask layer display issue. https://gitlab.com/kicad/code/kicad/-/issues/15642[#15642]

=== Command Line Interface
- https://gitlab.com/kicad/code/kicad/-/commit/691931f74dbd75861a1544b9bbe40886f2b52553[Add missing drill shape option when plotting board to SVG and PDF].
- https://gitlab.com/kicad/code/kicad/-/commit/5c5e41500a2f11b69e7e51c98f77da1d79de1194[Add option to control oval drill export behavior].

=== Windows
- https://gitlab.com/kicad/code/kicad/-/commit/0cc25f10c65675701e3453149716cbd095d67362[Update ngspice to version 41].
