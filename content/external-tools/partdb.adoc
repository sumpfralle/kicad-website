+++
title = "Part-DB"
toolimage = "/img/external-tools/partdb.png"
tooldeveloper = "Jan Böhmer (@jbtronics) and others"
toolurl = "https://github.com/Part-DB/Part-DB-server"
+++

Part-DB is an open-source web-based inventory management system for electronic components.
It supports the HTTP library format of KiCad 8+, meaning the components in available your storage can be directly viewed and placed inside KiCad. This is especially useful in teams, where Part-DB can be used as a central database of available components for designing PCBs and what symbols and footprints should be used for them in KiCad.