+++
title = "Picuno"
projectdeveloper = "Atul Ravi"
projecturl = "https://esccrasci.in/picuno/"
"made-with-kicad/categories" = [
    "Development Board "
]
+++

The Picuno is the mix of the RP2040 and the Arduino UNO. A drop in replacement for the UNO compatible with C and python derivatives.