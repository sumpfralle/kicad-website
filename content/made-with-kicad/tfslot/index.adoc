+++
title = "TFSLOT01A: Drone indicated airspeed sensor"
projectdeveloper = "ThunderFly s.r.o."
projecturl = "https://github.com/ThunderFly-aerospace/TFSLOT01"
"made-with-kicad/categories" = [
    "Drones"
]
+++

TFSLOT is Low speed airspeed sensor for UAVs. The device is equipped with electronics, which contains the required differential pressure sensor and an IMU unit. The sensor can be used as an external compass for the autopilot and it can improve the estimation of orientation in space. Accelerometer data, on the other hand, can be used to check vibrations. In contrast to classical Pitot tube, the TFSLOT design is perfect choice for low airspeed measurement.
